"use strict";

const btns = document.querySelectorAll(".btn");
let rememberedKey = btns[0];

document.addEventListener('keydown', e => {
    rememberedKey.style.backgroundColor = null;
    btns.forEach(item => {

        function changeColor (letter) {
            letter.style.backgroundColor = "blue"
        }

        if (e.code == item.dataset.key) {
            changeColor (item);
            rememberedKey = item;
        }
    });
});



